package homework1;

public class Main {


	public static void hillClimbing(){
		boolean ok = false;
		int iteration = 0;
		int[] v = generateRandomBitString(25);
	//	int[] v = {0,1,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,1,0,1,1,1,0,1};
		int counter = 0;
	//	do {
			ok = false;
			do{
				int[] vc = selectNeighborOf(v);
				double vcCamel = camel(vc);
				double vCamel = camel(v);
				if(Double.compare(vcCamel, vCamel) < 0){
					System.out.println(vcCamel + "..." + vCamel);
					v = vc;
					iteration = 0;
					ok = true;
				}
				iteration ++;

			}while(iteration < 50);
			counter++;

	//	}while(ok && counter < 100);

		//System.out.println(v.toString());
		show(v);

	}

	private static void show(int[] v) {
		System.out.println("----------------------------");
		for(int i=0; i< v.length; i++) {
			System.out.print(v[i]);
		}
		System.out.println("----------------------------");
	}

	private static int byteToInt(int[] x){
		int transformed = 0;
		for(int i = x.length - 1; i >=0; i--){
			transformed = transformed + (int)(Math.pow(2, x.length - 1 - i) * x[i]);
		}

		return transformed;
	}
	private static double camelCompute(double x1, double x2) {
		double firstPart, secondPart;
		firstPart = (4 - 2.1 * x1 * x1 + Math.pow(x1, 4)/3) * x1 * x1;
		secondPart = x1 * x2 + (-4 + 4 * x2 * x2) * x2 * x2;
		return firstPart + secondPart;
	}

	public static double camel(int[] v) {
		//x1 - 13 bits
		//x2 - 12 bits

		int[] x1Bits = extractX(v, 0, 13);
		int[] x2Bits = extractX(v, 13, v.length);

		double x1Ten = byteToInt(x1Bits);
		double x2Ten = byteToInt(x2Bits);


		double x1 = computeX(x1Ten, -3, 3);
		double x2 = computeX(x2Ten, -2, 2);

	//	System.out.println("x1:" + x1);
//		System.out.println("x2:" + x2);

		double computed = camelCompute(x1, x2);
//		System.out.println("Computer: " + computed);
//		System.out.println("------------------------------------------------");
		return computed;
	}

	private static double computeX(double xTen, double a, double b) {
		double N = b == 3? 6000: 4000;
		double n = Math.ceil(Math.log10(N) / Math.log10(2));

		return a + xTen * (b-a) / (Math.pow(2, n) - 1 );
	}

	private static int[] extractX(int[] v, int start, int end) {
		int[] x = new int[end-start];
		for(int i=start; i< end; i++ ){
			x[i-start] = v[i];
		}
		return x;
	}

	private static int mutation(int x){
		if(x == 0){
			return 1;
		}
		return 0;
	}

	private static int[] selectNeighborOf(int[] v) {
	//	System.out.println(byteToInt(v));
		int randomPoz = (int) (Math.random() * v.length);
	//	v[randomPoz] = mutation(v[randomPoz]);

		int[] v1 = new int[25];
		for(int i=0; i< 25; i++) {
			if(i == randomPoz) {
				v1[randomPoz] = mutation(v[randomPoz]);
			}else{
				v1[i] = v[i];
			}
		}
		return v1;
	}

	public static int[] generateRandomBitString(int n) {
		int[] v = new int[25];
		for(int i=0; i<n; i++) {
			v[i] = (Math.random() > 0.5 ? 1 : 0);
		}
		return v;
	}

	public static void main(String[] args) {


		hillClimbing();
		//System.out.println(camelCompute(-0.0898, 0.7126));
		}
		
	}
	
