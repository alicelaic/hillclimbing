package homework1;

import java.util.ArrayList;
import java.util.List;

public class MainGriewangsk {


	//pe toti  ii reprezentam pe 14 biti - precizie 1
	public static void hillClimbing(int n){
		boolean ok = false;
		int iteration = 0;
		int[] v = generateRandomBitString(14 * n);
		int counter = 0;
	//	do {
			ok = false;
			do{
				int[] vc = selectNeighborOf(v, n);
				double vcG = griewangsk(vc, n); //random neighbor
				double vG = griewangsk(v, n);
				if(Double.compare(vcG, vG) < 0){
					System.out.println(vcG + "..." + vG);
					v = vc;
					ok = true;
				}
				iteration ++;

			}while(iteration < 1000);
			counter++;

	//	}while(ok && counter < 100);

		//System.out.println(v.toString());
		show(v);

	}


	private static void show(int[] v) {
		System.out.println("----------------------------");
		for(int i=0; i< v.length; i++) {
			System.out.print(v[i]);
		}
		System.out.println("----------------------------");
	}

	private static int byteToInt(int[] x){
		int transformed = 0;
		for(int i = x.length - 1; i >=0; i--){
			transformed = transformed + (int)(Math.pow(2, x.length - 1 - i) * x[i]);
		}

		return transformed;
	}

	//everyone will be equally on 14 bits
	public static double griewangsk(int[] v, int n) {
		List<Double> xes = new ArrayList<>();
		for(int i=0;i<n;i++) {
			int[] x = extractX(v, 14*i, 14 * (i+1));
			double xTen = byteToInt(x);
			double xComputed = computeX(xTen, -600, 600);
			xes.add(xComputed);
		}

		double computed = computeGriewangsk(xes);


		return computed;
	}

	private static double computeGriewangsk(List<Double> xes) {
		double sum = 0;
		double prod = 1;
		for(int i=0; i<xes.size(); i++){
			sum = sum + Math.pow(xes.get(i), 2) / 4000;
			prod = prod * Math.cos(xes.get(i) / Math.sqrt(i+1));
		}

		return sum - prod + 1;

	}


	private static double computeX(double xTen, double a, double b) {
		double N = 12000; // (b-a) * 10^d
		double n = Math.ceil(Math.log10(N) / Math.log10(2));

		return a + xTen * (b-a) / (Math.pow(2, n) - 1 );
	}

	private static int[] extractX(int[] v, int start, int end) {
		int[] x = new int[end-start];
		for(int i=start; i< end; i++ ){
			x[i-start] = v[i];
		}
		return x;
	}

	private static int mutation(int x){
		if(x == 0){
			return 1;
		}
		return 0;
	}

	private static int[] selectNeighborOf(int[] v, int n) {
		int[] v1 = copyV(v);

		for(int j = 0; j<n; j++) {
			int randomPoz = (int) (Math.random() * v.length);
			v1[randomPoz] = mutation(v[randomPoz]);
		}
		return v1;
	}

	private static int[] copyV(int[] v) {
		int[] v1 = new int[v.length];
		for (int i = 0; i < v.length; i++) {
				v1[i] = v[i];
		}

		return v1;
	}

	public static int[] generateRandomBitString(int n) {
		int[] v = new int[n];
		for(int i=0; i<n; i++) {
			v[i] = (Math.random() > 0.5 ? 1 : 0);
		}
		return v;
	}

	public static void main(String[] args) {
	//	Double[] x = {Double.valueOf(-200), Double.valueOf(300), 500.0, 600.0 };
//		System.out.println(computeGriewangsk(Arrays.asList(x)));
		hillClimbing(5);
		//System.out.println(camelCompute(-0.0898, 0.7126));
		}
		
	}
	
