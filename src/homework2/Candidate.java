package homework2;

/**
 * Created by Alice on 3/7/2016.
 */
public class Candidate {
    private int[] bytes;

    public Candidate() {
    }

    public Candidate(int[] bytes) {
        this.bytes = bytes;
    }

    public int[] getBytes() {
        return bytes;
    }

    public void setBytes(int[] bytes) {
        this.bytes = bytes;
    }
}
