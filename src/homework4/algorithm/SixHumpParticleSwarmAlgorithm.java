package homework4.algorithm;

import homework4.Particle;
import homework4.Position;
import homework4.Velocity;
import homework4.functions.SixHumpProblemSet;
import homework4.particles.SixHumpParticle;

import java.util.Random;
import java.util.Vector;

import static homework4.functions.SixHumpProblemSet.PROBLEM_DIMENSION;
import static homework4.utils.Constants.*;

/**
 * @author alaic
 */
public class SixHumpParticleSwarmAlgorithm {
    private static Random generator = new Random();
    private static Vector<Particle> swarm = new Vector<>();
    private static double[] pBest = new double[SWARM_SIZE];
    private static Vector<Position> pBestPosition = new Vector<Position>();
    private static double gBest;
    private static  Position gBestPosition;
    private static double[] fitnessValueList = new double[SWARM_SIZE]; //contains fitness values for all swarm_size
    private static double w1; //ponderea inertiei - asigura echilibrul intre explorare/exploatare; este utilizata si ca pondere descrescatoare in timp (ca in Simulted Annealing)

    public static void main(String ... args){

        initSwarm();
        updateFitnessList(); //add fitness to a list for further use


        //initial fitness es and positions
        for(int i=0; i<SWARM_SIZE; i++) {
            pBest[i] = fitnessValueList[i];
            pBestPosition.add(swarm.get(i).getPosition());
        }


        int t=0;
        double evaluation;
        while(t < MAX_ITERATION){


            //update p (best position encountered) - based on the entire population (SWARM_SIZE)
            for(int i=0; i<SWARM_SIZE; i++) {
                if(fitnessValueList[i] < pBest[i]) {
                    pBest[i] = fitnessValueList[i]; //best fitness
                    pBestPosition.set(i, swarm.get(i).getPosition()); //best position according to fitness - replaced the old value
                }
            }


            // and g (best particle from the population)
            int bestParticleIndex = getMinPosition(fitnessValueList);
            if(t == 0 || fitnessValueList[bestParticleIndex] < gBest) {
                gBest = fitnessValueList[bestParticleIndex];
                gBestPosition = swarm.get(bestParticleIndex).getPosition();
            }


            w1 = W_MAX - (((double) t) / MAX_ITERATION) * (W_MAX - W_MIN);
            for(int i=0; i<SWARM_SIZE; i++) {
                double r1 = generator.nextDouble();
                double r2 = generator.nextDouble();

                Particle p = swarm.get(i);

                // step 3 - update velocity
                double[] newVel = new double[PROBLEM_DIMENSION];
                for(int j=0; j < PROBLEM_DIMENSION; j++) {
                    //p.getVelocity().getVelocity()[j]) is the old one ( -1)
                    newVel[j] = (w1 * p.getVelocity().getVelocity()[j]) +
                            (r1 * W2) * (pBestPosition.get(i).getPosition()[j] - p.getPosition().getPosition()[j]) +
                            (r2 * W3) * (gBestPosition.getPosition()[j] - p.getPosition().getPosition()[j]);
                }

                Velocity vel = new Velocity(newVel);
                p.setVelocity(vel);

                // step 4 - update location
                double[] newPos = new double[PROBLEM_DIMENSION];
                for(int j=0; j < PROBLEM_DIMENSION; j++) {
                    //p.getPosition().getPosition()[j] is the old one (-1)
                    newPos[j] = p.getPosition().getPosition()[j] + newVel[j];
                }
                Position position = new Position(newPos);
                p.setPosition(position);
            }


            //evaluate P(t) - swarm.get(t)
            evaluation = SixHumpProblemSet.evaluate(gBestPosition);
            System.out.println("Iteration " + t + ": ");
            System.out.println("-------Best X: " + gBestPosition.getPosition()[0]);
            System.out.println("-------Best Y: " + gBestPosition.getPosition()[1]);
            System.out.println("-------Fitness value: " + evaluation);
            t = t + 1;
            updateFitnessList();
        }

    }


    public static void initSwarm() {
        Particle p;
        for(int i = 0; i< SWARM_SIZE; i++) {
            p = new SixHumpParticle();

            // randomize position inside a space defined in Problem Set
            double[] pos = new double[PROBLEM_DIMENSION];
            pos[0] = SixHumpProblemSet.X_MIN + generator.nextDouble() * (SixHumpProblemSet.X_MAX - SixHumpProblemSet.X_MIN);
            pos[1] = SixHumpProblemSet.Y_MIN + generator.nextDouble() * (SixHumpProblemSet.Y_MAX - SixHumpProblemSet.Y_MIN);
            Position position = new Position(pos);

            // randomize velocity in the range defined in Problem Set
            double[] vel = new double[PROBLEM_DIMENSION];
            vel[0] = SixHumpProblemSet.VEL_LOW + generator.nextDouble() * (SixHumpProblemSet.VEL_HIGH - SixHumpProblemSet.VEL_LOW);
            vel[1] = SixHumpProblemSet.VEL_LOW + generator.nextDouble() * (SixHumpProblemSet.VEL_HIGH - SixHumpProblemSet.VEL_LOW);
            Velocity velocity = new Velocity(vel);

            p.setPosition(position); //generate positions x
            p.setVelocity(velocity); //generate velocity(viteza) vectors v
            swarm.add(p);
        }
    }


    public static void updateFitnessList() {
        for(int i=0; i<SWARM_SIZE; i++) {
            fitnessValueList[i] = swarm.get(i).getFitness();
        }
    }

    private static int getMinPosition(double[] arr){
        double minValue = arr[0];
        int minPosition = 0;
        for(int i=1; i<arr.length ;i++){
            if(arr[i] < minValue) {
                minValue = arr[i];
                minPosition = i;
            }
        }

        return minPosition;
    }
}
