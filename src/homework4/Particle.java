package homework4;

/**
 * @author alaic
 */
public abstract class Particle {
    private double fitness;
    private Velocity velocity;
    private Position position;

    public Particle() {
    }

    public Particle(Position position, Velocity velocity, double fitness) {
        this.position = position;
        this.velocity = velocity;
        this.fitness = fitness;
    }

    public abstract double getFitness();

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

    public Velocity getVelocity() {
        return velocity;
    }

    public void setVelocity(Velocity velocity) {
        this.velocity = velocity;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
