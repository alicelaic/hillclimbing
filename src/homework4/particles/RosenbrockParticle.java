package homework4.particles;

import homework4.Particle;
import homework4.functions.RosenbrockProblemSet;

/**
 * @author alaic
 */
public class RosenbrockParticle extends Particle {
    @Override
    public double getFitness() {
        double fitness = RosenbrockProblemSet.evaluate(this.getPosition());
        this.setFitness(fitness);
        return fitness;
    }
}
