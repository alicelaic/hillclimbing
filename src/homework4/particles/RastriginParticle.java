package homework4.particles;

import homework4.Particle;
import homework4.functions.RastriginProblemSet;

/**
 * @author alaic
 */
public class RastriginParticle extends Particle{

    @Override
    public double getFitness() {
        double fitness = RastriginProblemSet.evaluate(this.getPosition());
        this.setFitness(fitness);
        return fitness;
    }

}
