package homework4.particles;

import homework4.Particle;
import homework4.functions.SixHumpProblemSet;

/**
 * @author alaic
 */
public class SixHumpParticle extends Particle {
    @Override
    public double getFitness() {
        double fitness = SixHumpProblemSet.evaluate(this.getPosition());
        this.setFitness(fitness);
        return fitness;
    }
}
