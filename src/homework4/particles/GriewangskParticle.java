package homework4.particles;

import homework4.Particle;
import homework4.functions.GriewanskProblemSet;

/**
 * @author alaic
 */
public class GriewangskParticle extends Particle{


    @Override
    public double getFitness() {
        double fitness = GriewanskProblemSet.evaluate(this.getPosition());
        this.setFitness(fitness);
        return fitness;
    }

}
