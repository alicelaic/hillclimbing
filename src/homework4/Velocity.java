package homework4;

/**
 *  Vectorul viteza este calculat tinand cont de:
 *   * fiecare particula tinde sa-si pastreze directia curenta (inertie)
 *   * fiecare particula este atrasa de cea mai buna pozitie pe care a atins-o pana acum p
 *   * fiecare particula este atrasa de catre cea mai buna particula din populatie g (sau din vecinatate)
 *
 *   @author alaic
 */
public class Velocity {
    private double[] velocity;

    public Velocity() {
    }

    public Velocity(double[] velocity) {
        this.velocity = velocity;
    }

    public double[] getVelocity() {
        return velocity;
    }

    public void setVelocity(double[] velocity) {
        this.velocity = velocity;
    }
}
