package homework4;

/**
 * Fiecare particula este reprezentata de un vector x de lungime n indicand pozitia in spatiul n-dimensional
 * @author alaic
 */
public class Position {
    private double[] position;

    public Position(double[] position) {
        this.position = position;
    }

    public Position() {
    }

    public double[] getPosition() {
        return position;
    }

    public void setPosition(double[] position) {
        this.position = position;
    }
}
