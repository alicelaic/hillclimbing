package homework4.functions;

import homework4.Position;

/**
 * f2(x)=sum(100·(x(i+1)-x(i)^2)^2+(1-x(i))^2)
 *  i=1:n-1; -2.048<=x(i)<=2.048.
 *
 * @author alaic
 */
public class RosenbrockProblemSet {
    public static final double X_MIN = -2.048; //this is minimum interval of x1
    public static final double X_MAX =2.048; //this is maximum interval of x1
    public static final double VEL_LOW = -1;
    public static final double VEL_HIGH = 1;
    public static final int PROBLEM_DIMENSION = 6;

    public static double evaluate(Position position) {
        double result = 0;

        for(int i=0; i < PROBLEM_DIMENSION-1; i++) {
            result = result + 100 * (Math.pow((position.getPosition()[i+1] - Math.pow(position.getPosition()[i], 2)), 2) + Math.pow((1 - position.getPosition()[i]), 2));
        }

        return result;
    }
}
