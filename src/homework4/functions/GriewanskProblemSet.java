package homework4.functions;

import homework4.Position;

/**
 * f8(x)=sum(x(i)^2/4000)-prod(cos(x(i)/sqrt(i)))+1, i=1:n
 * -600<=x(i)<= 600.
 *
 * @author alaic
 */
public class GriewanskProblemSet {
    public static final double X_MIN = -600; //this is minimum interval of x1
    public static final double X_MAX = 600; //this is maximum interval of x1
    public static final double VEL_LOW = -1;
    public static final double VEL_HIGH = 1;
    public static final int PROBLEM_DIMENSION = 6;

    public static double evaluate(Position position) {
        double result = 0;
        double sum = 0;
        double multiply = 1;
        for(int i=0; i < PROBLEM_DIMENSION; i++) {
            sum = sum + (Math.pow(position.getPosition()[i], 2)/4000);
        }

        for(int i=0; i < PROBLEM_DIMENSION; i++) {
            multiply = multiply * (Math.cos(position.getPosition()[i]/ Math.sqrt(i+1)));
        }

        result = sum - multiply + 1;
        return result;
    }
}
