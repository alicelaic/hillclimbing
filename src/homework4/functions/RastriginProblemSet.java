package homework4.functions;

import homework4.Position;

/**
 * f6(x)=10·n+sum(x(i)^2-10·cos(2·pi·x(i))), i=1:n;
 * -5.12<=x(i)<=5.12.
 *
 * @author alaic
 */
public class RastriginProblemSet {
    public static final double X_MIN = -5.12; //this is minimum interval of x1
    public static final double X_MAX = 5.12; //this is maximum interval of x1
    public static final double VEL_LOW = -1;
    public static final double VEL_HIGH = 1;
    public static final int PROBLEM_DIMENSION = 6;

    public static double evaluate(Position position) {
        double result = 10 * PROBLEM_DIMENSION;

        for(int i=0; i < PROBLEM_DIMENSION; i++) {
            result = result + Math.pow(position.getPosition()[i], 2) - 10 * Math.cos(2 * Math.PI * position.getPosition()[i]);
        }

        return result;
    }
}
