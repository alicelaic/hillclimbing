package homework4.functions;

import homework4.Position;

/**
 *  fSixh(x1,x2)=(4-2.1·x1^2+x1^4/3)·x1^2+x1·x2+(-4+4·x2^2)·x2^2
 *       -3<=x1<=3, -2<=x2<=2.
 * @author alaic
 */
public class SixHumpProblemSet {
    public static final double X_MIN = -3; //this is minimum interval of x1
    public static final double X_MAX = 3; //this is maximum interval of x1
    public static final double Y_MIN = -2;
    public static final double Y_MAX = 2;
    public static final double VEL_LOW = -1;
    public static final double VEL_HIGH = 1;
    public static final int PROBLEM_DIMENSION = 2; //this is n

    public static double evaluate(Position position) {
        double result = 0;
        double x1 = position.getPosition()[0]; // the "x" part of the location
        double x2 = position.getPosition()[1]; // the "y" part of the location

        result = (4 - 2.1 * Math.pow(x1, 2) + Math.pow(x1, 4/3)) * Math.pow(x1, 2) +
                x1 * x2 + (-4 + 4 * Math.pow(x2, 2)) * Math.pow(x2, 2);

        return result;
    }
}
