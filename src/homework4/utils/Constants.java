package homework4.utils;

/**
 * @author alaic
 */
public class Constants {
    public static final int SWARM_SIZE = 30;
    public static final int MAX_ITERATION = 100;

    public static final double W2 = 2.0; //factor de invatare - parametru cognitiv - tendinta de a duplica actiuni trecute proprii care s-au dovedit de succes
    public static final double W3 = 2.0; //factor de invatare - parametru social - tendinta de a urma succesul celorlalti indivizi

    public static final double W_MIN = 0.0;
    public static final double W_MAX = 1.0;
}
