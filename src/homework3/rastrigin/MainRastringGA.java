package homework3.rastrigin;

import homework1.MainRastring;
import homework2.Candidate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Alice on 3/7/2016.
 */
public class MainRastringGA {
    public static final int POP_SIZE = 50;
    public static final Double mutationProb = 0.25;
    public static final Double crossoverProb = 0.3;
    public static final int HOW_MANY = 30;

    public static List<Candidate> ga(List<Candidate> population) {
        List<Double> eval = new ArrayList<>();
        List<Double> p = new ArrayList<>();
        List<Double> q = new ArrayList<>();
        double bestFitnessValue = Double.MAX_VALUE;
        int bestFitnessIndex = -1;
        List<Candidate> selected = new ArrayList<>();
        //evaluate p
        for (int i = 0; i < population.size(); i++) {
            //choose best local fitness
            double fitnessLocal = fitness(population.get(i));
            if(Double.compare(fitnessLocal, bestFitnessValue) < 0) {
                bestFitnessValue = fitnessLocal;
                bestFitnessIndex = i;
            }
            eval.add(fitnessLocal);
        }
        System.out.println("Cel mai bun: " + bestFitnessIndex + ": " + bestFitnessValue);

        double total = 0;
        //total fitness
        double min = Double.MIN_VALUE;
        for (int i = 0; i < population.size(); i++) {
            if(Double.compare(eval.get(i), min) > 0){
                min = eval.get(i);
            }
            total = total + eval.get(i);
        }

        System.out.println("Total fitness: " + 1.0/min);

        //individual selection
        for (int i = 0; i < population.size(); i++) {
            p.add(eval.get(i) / total);
        }

        //cummulate selection
        q.add(0.0);
        for (int i = 1; i < population.size(); i++) {
            q.add(q.get(i - 1) + p.get(i));
        }

        //selection
        for (int i = 0; i < population.size(); i++) {
            double r = Math.random();
            int j = getPosition(q, r);
            if (j != -1) {
                selected.add(new Candidate(population.get(j).getBytes()));
            }

        }


        /*calculate again*/
        List<Double> eval1 = new ArrayList<>();
        for (int i = 0; i < selected.size(); i++) {

            eval1.add(fitness(population.get(i)));
        }

        double min2 = Double.MIN_VALUE;
        double total2 = 0.0;
        for (int i = 0; i < selected.size(); i++) {
            if(Double.compare(eval1.get(i), min2) > 0){
                min2 = eval1.get(i);
            }
            total2 = total2 + eval1.get(i);
        }

        //System.err.println("Min: " + 1/min2 + " ...total: " + total2);

        /*end calculate again*/

        List<Candidate> selectedForCrossover = selected;
        Map<Integer, Boolean> suitableForCrossover = new HashMap<>();
        //select others for crossover
        for (int i = 0; i < selectedForCrossover.size(); i++) {
            double r = Math.random();
            if (Double.compare(r, crossoverProb) < 0) {
                suitableForCrossover.put(i, true);
                //   selectedForCrossover.add(selected.get(i));
            }

        }
        //end select
        List<Candidate> newCandidates = new ArrayList<>();
        //incrucisare 2 cate 2
        for (int i = 1; i < selectedForCrossover.size(); i = i + 2) {
            int suitable = getSuitableForCrossover(suitableForCrossover, i - 1, i);
            newCandidates.addAll(crossover(selectedForCrossover.get(i - 1), selectedForCrossover.get(i), suitable));
        }


        List<Candidate> newCandidatesAfterMutation = new ArrayList<>();
        //incrucisare 2 cate 2
        for (int i = 0; i < newCandidates.size(); i++) {
            if(bestFitnessIndex != i) {
                newCandidatesAfterMutation.add(mutation(newCandidates.get(i)));
            }else {
                newCandidatesAfterMutation.add(newCandidates.get(i));
            }
        }
        //then select for mutation

        System.out.println("Size: " + newCandidatesAfterMutation.size());
        return newCandidatesAfterMutation;
    }

    private static int getSuitableForCrossover(Map<Integer, Boolean> suitableForCrossover, int i, int i1) {
        boolean first = suitableForCrossover.get(i) == null ? false : true;
        boolean second = suitableForCrossover.get(i1) == null ? false : true;

        if (first && second) {
            return 3;
        } else if (!first && second) {
            return 2;
        } else if (first && !second) {
            return 1;
        } else {
            return 0;
        }
    }

    private static Candidate mutation(Candidate candidate) {
     /*   int[] candidateBytes = candidate.getBytes();
        for (int i = 0; i < candidateBytes.length; i++) {
            double r = Math.random();
            if (Double.compare(r, mutationProb) < 0) {
                candidateBytes[i] = not(candidateBytes[i]);
            }
        }
*/
        int[] candidateBytes = homework3.rastrigin.MainRastring.hillClimbing(candidate.getBytes());
        return new Candidate(candidateBytes);
    }

    private static int not(int candidateByte) {
        if (candidateByte == 1) {
            return 0;
        }
        return 1;
    }

    private static List<Candidate> crossover(Candidate c1, Candidate c2, int replace) {
        int cutPoint = (int) (Math.random() * c1.getBytes().length);

        int[] cc1, cc2, ccc1, ccc2;
        int ssize = c1.getBytes().length;
        cc1 = new int[ssize];
        cc2 = new int[ssize];

        ccc1 = c1.getBytes();
        ccc2 = c2.getBytes();

        for (int i = 0; i < ssize; i++) {
            if (i <= cutPoint) {
                cc1[i] = ccc2[i];
                cc2[i] = ccc1[i];
            } else {
                cc1[i] = ccc1[i];
                cc2[i] = ccc2[i];
            }
        }

        List<Candidate> newCandidates = new ArrayList<>();
        switch (replace) {
            case 0:
                //none of them should be replaced
                newCandidates.add(c1);
                newCandidates.add(c2);
                break;
            case 1:
                //only first one should be replaced
                newCandidates.add(new Candidate(cc1));
                newCandidates.add(c2);
                break;
            case 2:
                //only second one should be replaced
                newCandidates.add(c1);
                newCandidates.add(new Candidate(cc2));
                break;
            case 3:
                //both should be replaced
                newCandidates.add(new Candidate(cc1));
                newCandidates.add(new Candidate(cc2));
                break;
            default:
                System.err.println("nooooooooooooooooooooo===============");
        }

        return newCandidates;

    }

    private static int getPosition(List<Double> q, double r) {
        if (Double.compare(r, q.get(0)) <= 0) {
            return 0;
        }
        for (int i = 1; i < q.size(); i++) {
            if (Double.compare(q.get(i), r) > 0 && Double.compare(r, q.get(i - 1)) >= 0) {
                return i - 1;
            }
        }

        if (Double.compare(q.get(q.size() - 1), r) <= 0) {
            return q.size() - 1;
        }

        return -1;
    }

    private static Double fitness(Candidate candidate) {
        return 1.0 / MainRastring.rastring(candidate.getBytes(), HOW_MANY);
    }


    private static void show(List<Candidate> resulted) {
        for (Candidate c : resulted) {
            for (int i = 0; i < c.getBytes().length; i++) {
                System.out.print(c.getBytes()[i]);
            }
            System.out.println();
            System.out.println("------------------------------");
        }
    }

    private static List<Candidate> generateInitialPopulation() {
        List<Candidate> candidates = new ArrayList<>();
        for (int i = 0; i < POP_SIZE; i++) {
            candidates.add(new Candidate(MainRastring.generateRandomBitString(14 * HOW_MANY)));
        }

        return candidates;

    }

    public static void main(String... args) {
        List<Candidate> initialPopulation = generateInitialPopulation();
        //    List<Candidate> resulted = ga(initialPopulation);
        for (int i = 0; i < 500; i++) {
            System.out.println("Step " + i + ": --------------");
            initialPopulation = ga(initialPopulation);
            System.out.println("-------------------------------");
        }
        //show(resulted);
    }

}
