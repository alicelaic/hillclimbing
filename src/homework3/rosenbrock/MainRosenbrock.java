package homework3.rosenbrock;

import java.util.ArrayList;
import java.util.List;

public class MainRosenbrock {


    //pe toti  ii reprezentam pe 12 biti - precizie 3
    public static int[] hillClimbing(int[] v) {
        boolean ok = false;
        int iteration = 0;
        int n = v.length / 12;
        int counter = 0;
        ok = false;
        do {
            int[] vc = selectNeighborOf(v, n);
            double vcG = rosenbrock(vc, n); //random neighbor
            double vG = rosenbrock(v, n);
            if (Double.compare(vcG, vG) < 0) {
                //	System.out.println(vcG + "..." + vG);
                v = vc;
                ok = true;
            }
            iteration++;

        } while (iteration < 10);
        counter++;

        return v;

    }


    private static void show(int[] v) {
        System.out.println("----------------------------");
        for (int i = 0; i < v.length; i++) {
            System.out.print(v[i]);
        }
        System.out.println("----------------------------");
    }

    private static int byteToInt(int[] x) {
        int transformed = 0;
        for (int i = x.length - 1; i >= 0; i--) {
            transformed = transformed + (int) (Math.pow(2, x.length - 1 - i) * x[i]);
        }

        return transformed;
    }

    //everyone will be equally on 12 bits
    public static double rosenbrock(int[] v, int n) {
        List<Double> xes = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            int[] x = extractX(v, 12 * i, 12 * (i + 1));
            double xTen = byteToInt(x);
            double xComputed = computeX(xTen, -2.048, 2.048);
            xes.add(xComputed);
        }

        double computed = computeRosenbrock(xes);


        return computed;
    }

    private static double computeRosenbrock(List<Double> xes) {
        double sum = 0;
        for (int i = 0; i < xes.size() - 1; i++) {
            sum = sum + 100 * (xes.get(i + 1) - xes.get(i) * xes.get(i)) * (xes.get(i + 1) - xes.get(i) * xes.get(i))
                    + (1 - xes.get(i)) * (1 - xes.get(i));
        }

        return sum;

    }


    private static double computeX(double xTen, double a, double b) {
        double N = 4096; // (b-a) * 10^d
        double n = Math.ceil(Math.log10(N) / Math.log10(2));

        return a + xTen * (b - a) / (Math.pow(2, n) - 1);
    }

    private static int[] extractX(int[] v, int start, int end) {
        int[] x = new int[end - start];
        for (int i = start; i < end; i++) {
            x[i - start] = v[i];
        }
        return x;
    }

    private static int mutation(int x) {
        if (x == 0) {
            return 1;
        }
        return 0;
    }

    private static int[] selectNeighborOf(int[] v, int n) {
        int[] v1 = copyV(v);

        for (int j = 0; j < n; j++) {
            int randomPoz = (int) (Math.random() * v.length);
            v1[randomPoz] = mutation(v[randomPoz]);
        }
        return v1;
    }

    private static int[] copyV(int[] v) {
        int[] v1 = new int[v.length];
        for (int i = 0; i < v.length; i++) {
            v1[i] = v[i];
        }

        return v1;
    }

    public static int[] generateRandomBitString(int n) {
        int[] v = new int[n];
        for (int i = 0; i < n; i++) {
            v[i] = (Math.random() > 0.5 ? 1 : 0);
        }
        return v;
    }

    public static void main(String[] args) {
        //	Double[] x = {  1.024 , 1.021 , 1.023 , 1.151 , 1.536 };
        //System.out.println(computeRosenbrock(Arrays.asList(x)));
        //hillClimbing(30);
        //System.out.println(camelCompute(-0.0898, 0.7126));
    }

}

